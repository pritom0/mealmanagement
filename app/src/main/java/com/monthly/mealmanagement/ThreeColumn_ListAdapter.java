package com.monthly.mealmanagement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ThreeColumn_ListAdapter extends ArrayAdapter<User> {

    private LayoutInflater mInflater;
    private ArrayList<User> users;
    private int mViewResourceId;

    public ThreeColumn_ListAdapter(Context context, int textViewResourceId, ArrayList<User> users) {
        super(context, textViewResourceId, users);
        this.users = users;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;


    }

    public View getView(int position, View convertView, ViewGroup parent){
            convertView = mInflater.inflate(mViewResourceId,null);

            User user= users.get(position);

            if ( user != null){
                TextView Name = (TextView) convertView.findViewById(R.id.textName);
                TextView Date = (TextView) convertView.findViewById(R.id.textDate);
                TextView Details = (TextView) convertView.findViewById(R.id.textDetails);

                if ( Name != null){
                    Name.setText(user.getName());
                }

                if ( Date != null){
                    Date.setText(user.getDate());
                }

                if ( Details != null ){
                    Details.setText(user.getDetails());
                }




            }

            return  convertView;

    }

}
