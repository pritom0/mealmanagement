package com.monthly.mealmanagement;

public class User {

    private  String Name;
    private String Date;
    private String Details;

    public  User (String eName, String eDate, String eDetails){
        Name = eName;
        Date = eDate;
        Details= eDetails;

    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }


}
