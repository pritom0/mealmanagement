package com.monthly.mealmanagement;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonsighnup;
    private EditText etemail;
    private EditText etpass;
    private TextView textlogin;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser()!= null){
            //profile activity
            finish();
            startActivity(new Intent(getApplicationContext(),ProfielActivity.class));

        }

        buttonsighnup=(Button)findViewById(R.id.btnlogin);
        etemail=(EditText)findViewById(R.id.temail);
        etpass=(EditText)findViewById(R.id.epass);
        textlogin=(TextView) findViewById(R.id.textlogin);

        progressDialog= new ProgressDialog(this);

        buttonsighnup.setOnClickListener(this);
        textlogin.setOnClickListener(this);

    }

    public  void userlogin()
    {
        String email = etemail.getText().toString().trim();
        String password = etpass.getText().toString().trim();


        if (TextUtils.isEmpty(email)){
            // email is empty

            Toast.makeText(this,"Please enter email",Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)){

            Toast.makeText(this,"please enter password",Toast.LENGTH_SHORT).show();
            return;
            // password is empty
        }


        progressDialog.setMessage("Registering User...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                      progressDialog.dismiss();
                      if (task.isSuccessful()){
                          //start profile activity
                          finish();
                          startActivity(new Intent(getApplicationContext(),ProfielActivity.class));
                      }

                    }
                });







    }


    @Override
    public void onClick(View v) {
      if (v == buttonsighnup){
          userlogin();
      }

      if (v == textlogin){
          finish();
          startActivity(new Intent(this,MainActivity.class));
      }
    }


}
