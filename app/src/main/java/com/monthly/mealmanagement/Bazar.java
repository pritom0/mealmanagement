package com.monthly.mealmanagement;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Bazar extends AppCompatActivity {

    EditText etname,etdate,etdetails;
    Button btnadd,btnview;
    DatabaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bazar);

        etdetails =(EditText)findViewById(R.id.etdetails);
        etdate = (EditText)findViewById(R.id.etdate);
        etname = (EditText)findViewById(R.id.etname);
        btnadd =(Button)findViewById(R.id.buttonadd);
        btnview=(Button)findViewById(R.id.buttonview);
        myDB = new DatabaseHelper(this);

        btnview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Bazar.this,ViewListContents.class);
                startActivity(intent);
            }
        });

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ename= etname.getText().toString();
                String edate = etdate.getText().toString();
                String edetails = etdetails.getText().toString();

                if (ename.length() != 0 && edate.length() != 0 && edetails.length() != 0){
                  AddData(ename,edate,edetails);
                  etname.setText("");
                  etdate.setText("");
                  etdetails.setText("");
                }else {
                    Toast.makeText(Bazar.this,"You Must Put Something on the Text Field!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void AddData(String name,String date,String details){
        boolean insertdata = myDB.addData(name,date,details);


        if(insertdata ==true){
            Toast.makeText(Bazar.this,"Successfully Entered Data!",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(Bazar.this,"Something went wrong :(",Toast.LENGTH_LONG).show();
        }
    }
}
