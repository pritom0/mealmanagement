package com.monthly.mealmanagement;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.util.ArrayList;

public class Members extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private EditText emember;
    private Button btnadd;
    private ListView memberlist;

    private ArrayList<String> member;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);

        emember=(EditText)findViewById(R.id.item_edit_name);
        btnadd=(Button)findViewById(R.id.addmember);
        memberlist=(ListView)findViewById(R.id.memeber_list);

        member = FileHelper.readData(this);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,member);
        memberlist.setAdapter(adapter);

        btnadd.setOnClickListener(this);
        memberlist.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.addmember:
             String memberEntered = emember.getText().toString();
             adapter.add(memberEntered);
             emember.setText("");
             FileHelper.writeData(member,this);
             Toast.makeText(this,"memeber added",Toast.LENGTH_SHORT).show();

          break;

      }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        member.remove(position);
        adapter.notifyDataSetChanged();
        Toast.makeText(this,"Delete",Toast.LENGTH_SHORT).show();

    }
}
