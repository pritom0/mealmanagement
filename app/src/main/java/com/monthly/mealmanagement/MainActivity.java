package com.monthly.mealmanagement;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonregister;
    private EditText etemail;
    private EditText etpass;
    private TextView textviewsighnin;
    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser()!= null){
            //profile activity
            finish();
            startActivity(new Intent(getApplicationContext(),ProfielActivity.class));

        }


        progressDialog = new ProgressDialog(this);

        buttonregister=(Button)findViewById(R.id.btnregister);
        etemail=(EditText)findViewById(R.id.etemail);
        etpass=(EditText)findViewById(R.id.etpass);
        textviewsighnin=(TextView)findViewById(R.id.textViewSignin);

        buttonregister.setOnClickListener(this);
        textviewsighnin.setOnClickListener(this);

    }

    private  void registerUser(){
        String email= etemail.getText().toString().trim();
        String password = etpass.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            // email is empty

            Toast.makeText(this,"Please enter email",Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)){

            Toast.makeText(this,"please enter password",Toast.LENGTH_SHORT).show();
            return;
            // password is empty
        }

        progressDialog.setMessage("Registering User...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            finish();
                            startActivity(new Intent(getApplicationContext(),ProfielActivity.class));


                        } else {
                            Toast.makeText(MainActivity.this,"Could Not Register",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {

        if ( v == buttonregister){
           registerUser();
        }
        if ( v == textviewsighnin){
            // open login activity

            startActivity(new Intent(this,LoginActivity.class));
        }

    }
}
