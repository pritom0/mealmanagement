package com.monthly.mealmanagement;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfielActivity extends AppCompatActivity implements View.OnClickListener  {

    private FirebaseAuth firebaseAuth;
    private TextView textviewuseremail;
    private Button buttonlogout;
    private Button buttondashboard;
    private  Button buttonbazar;
    private  Button buttonmembers;
    private  Button buttonsavings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiel);

        buttondashboard=(Button)findViewById(R.id.btndashboard);
        buttonbazar = (Button)findViewById(R.id.btnbazar);
        buttonmembers = (Button)findViewById(R.id.btnmembers);
        buttonsavings = (Button)findViewById(R.id.btnsavings);



        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }

        FirebaseUser user = firebaseAuth.getCurrentUser();
        textviewuseremail = (TextView)findViewById(R.id.textViewuseremail);
        textviewuseremail.setText("Welcome "+user.getEmail());
        buttonlogout = (Button)findViewById(R.id.buttonlogout);

        buttonlogout.setOnClickListener(this);
        buttondashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDashboard();
            }
        });

        buttonbazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBazar();
            }
        });

        buttonmembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMembers();
            }
        });


    }


    public void openBazar()
    {
        Intent intent = new Intent(this,Bazar.class);
        startActivity(intent);
    }

    public void openMembers(){
        Intent intent = new Intent(this,Members.class);
        startActivity(intent);
    }

    public void openDashboard(){
        Intent intent = new Intent(this,Dashboard.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
     if (v == buttonlogout){
         firebaseAuth.signOut();
         finish();
     }
    }
}
